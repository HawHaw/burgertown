import { StyleSheet, SafeAreaView, Platform, Text, TouchableOpacity } from 'react-native';
import  Constants  from 'expo-constants';
import MenuBar from "./assets/Screens/MenuBar"
import ListOfFood from './assets/Screens/ListOfFood';
import colors from './assets/config/Colors';
import FloatingButton from './assets/components/FloatingButton';


export default function App() { 
  return (
    <SafeAreaView style={styles.container}>
      {/* need to make a title for it */}
      <Text style={{fontSize: 30,justifyContent: "center" ,alignSelf: "center"}}>
         BurgerTown</Text>
        <MenuBar />
       <ListOfFood/>
      <FloatingButton title={"Cart"}/>
   
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.primary,
    paddingTop: Constants.statusBarHeight + 10  

  },

});
