// background-image: linear-gradient(260deg, #4b7c9c 0%, #516fd1 100%);

export default {
  primary: "#516fd1",
  secondary: "#6485f5",
  black: "#000",
  white: "#fff",
  medium: "#6e6969",
  light: "#4f75f7",
  danger: "#ff5252",
};
