import { StyleSheet, Text, View, TouchableOpacity } from 'react-native'
import React from 'react'
import Button from '../components/Button';
import Colors from '../config/Colors';

export default function MenuBar() {
  return (
    <View style={styles.bar}>
        <Button label={"Meun"}/>
        <Button label={"Checkout"}/>
        <Button label={"Contact us!"}/>
    </View>
  )
}

const styles = StyleSheet.create({
    bar:{
        flexDirection: "row",
        justifyContent: "space-around",
        alignSelf: "center",
        width: "90%",
        padding: 10,
        margin: 10,
        borderRadius: 35,
        
        backgroundColor: Colors.secondary,
        
        // fontSize: 10,
        // borderWidth: 0.5,
        // borderColor: "black",
    },
});

