import { StyleSheet, Text, View, ScrollView, TouchableOpacity, } from 'react-native'
import React, { useState } from "react";
import BackgroundLayout from '../components/BackgroundLayout';


const ingredientsList = [
    {
      id: 1,
      name: "Sesame Bun  ",
      price: 1,
      image: require("../images/bun.jpg")

    }, {
        id: 2,
        name: "Bun",
        price: 1,
        image: require("../images/bun3.jpg")
  
      },
      {
        id: 3,
        name: "Lettuce",
        price: 1,
        image: require("../images/lettuce.jpg")
  
      },
      {
        id: 4,
        name: "BBQ Sauce",
        price: 1,
        image: require("../images/bbq.jpg")
  
      },
      {
        id: 5,
        name: "Mayo Sauce",
        price: 1,
        image: require("../images/mayo.jpg")
  
      },
      {
        id: 6,
        name: "Dijon Sauce",
        price: 1,
        image: require("../images/dijon.jpg")
  
      }, 
      {
        id: 7,
        name: "Spicy Sauce",
        price: 1,
        image: require("../images/spicy.jpg")
  
      },
      {
        id: 8,
        name: "Beef Patty",
        price: 1,
        image: require("../images/beef.jpg")
  
      },
      {
        id: 9,
        name: "Chicken Patty",
        price: 1,
        image: require("../images/chicken.jpg")
  
      },
      {
        id: 10,
        name: "Tofu Patty",
        price: 1,
        image: require("../images/tofu.jpg")
  
      },
      {
        id: 11,
        name: "Turkey Patty",
        price: 1,
        image: require("../images/turkey.jpg")
  
      },
      {
        id: 12,
        name: "Cheese",
        price: 1,
        image: require("../images/cheese.png")
  
      },
      {
        id: 13,
        name: "Bacon",
        price: 1,
        image: require("../images/bacon.jpg")
  
      },
      {
        id: 14,
        name: "Tomato",
        price: 1,
        image: require("../images/tomato.jpg")
  
      },
      {
        id: 15,
        name: "Onion",
        price: 1,
        image: require("../images/onion.jpg")
  
      }, 
      {
        id: 16,
        name: "Onion Ring",
        price: 1,
        image: require("../images/friedonion.jpg")
  
      },
      {
        id: 17,
        name: "Fries",
        price: 1,
        image: require("../images/fries.jpg")
  
      },
      {
        id: 18,
        name: "Poutine",
        price: 1,
        image: require("../images/poutine.jpg")
  
      },
      {
        id: 19,
        name: "Salad",
        price: 1,
        image: require("../images/salad.jpg")
  
      },
     
];


// const testing = [
//   {
//     title: "Buns",
//     function: ingredients.filter((obj) => obj.id <= 3 )
//   },
//   {
//     title: "Buns2",
//     function: ingredients.filter((obj) => obj.id <= 3 )
//   }
// ];



export default function FoodList( props) {
    const [ingredients, setIngredientsList] = useState(ingredientsList);

    const buns    = ingredients.filter((obj) => obj.id <= 3 );
    const sauce   = ingredients.filter((obj) => obj.id > 3 && obj.id < 8);
    const topping = ingredients.filter((obj) => obj.id > 11 && obj.id < 16);
    const patties = ingredients.filter((obj) => obj.id > 7 && obj.id < 12);
    const sides   = ingredients.filter((obj) => obj.id > 15 && obj.id < 20);

  return (

    <ScrollView contentContainerStyle={ {paddingBottom: 60} } showsVerticalScrollIndicator={false}>
      <View>

      <BackgroundLayout  
        title={"Buns"} 
        data={buns}/>

      <BackgroundLayout  
        title={"Sauce"} 
        data={sauce}
        choose/>

      <BackgroundLayout  
        title={"Topping"} 
        data={topping}
        />

      <BackgroundLayout  
        title={"Patties"} 
        data={patties}/>

      <BackgroundLayout  
        title={"Sides"} 
        data={sides}/>
        </View>
  
    </ScrollView> 

    
 
  )  
}

