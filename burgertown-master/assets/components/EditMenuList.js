import { StyleSheet, Image, View, TouchableOpacity, Text } from 'react-native'
import React from 'react'
import { AntDesign } from '@expo/vector-icons';
import colors from '../config/Colors';
import PlusNminus from './PlusNminus';
import RadioBotton from './RadioBotton';

export default function MenuList({name, price, image, choose}) {
  return (
    
    <View style={styles.container}>
        {image && <Image style={styles.image} source={image}/> }
        <View style={styles.details}>

          <View style={styles.info}>
            <Text style={styles.infoText}>{name}</Text>
            <Text style={styles.infoText}>${price}</Text>
          </View>
          <View>
            {choose ? <RadioBotton/> : <PlusNminus/>}
          </View>

        </View>
    </View>
    
  )
}

const styles = StyleSheet.create({
    container: {
      margin: 10,
      flexDirection: "row",
      padding: 5,
      width: "85%",
      backgroundColor:  colors.light, //#6485f5
      alignSelf: "center",
      borderRadius: 45,
    },
    image:{
        width: 85,
        height: 85,
        borderRadius: 35,
        marginRight: 20
    },
    details:{
      // paddingLeft: 10,
      width: "65%",
      flexDirection: "column",
      alignSelf: "center",
      // backgroundColor: "red"
    },
    info:{
      width: "80%",
      flexDirection: "row",
      // marginLeft: 20,
      paddingBottom: 15,
      justifyContent: "space-between",
      // backgroundColor:"red",
    },
   infoText:{
     fontSize: 19
   },

})