import { StyleSheet, Text, View } from 'react-native'
import React, { useState } from 'react'
import BouncyCheckbox from "react-native-bouncy-checkbox";
import Colors from '../config/Colors';

// https://www.npmjs.com/package/react-native-bouncy-checkbox
export default function RadioBotton() {
    const [choose, setChoose] = useState(1);
  return (
    <View style={styles.container}> 
        <BouncyCheckbox 
            text='Light' 
            onPress={()=> {setChoose(0)}} 
            disableBuiltInState={choose != 0} 
            textStyle={{color:"white", right:12, fontSize:12, textDecorationLine: 'none'}} 
            style={{right:16}}
            fillColor={Colors.secondary} 
            unfillColor={Colors.primary}
            />

        <BouncyCheckbox
            text='Normal' 
            isChecked={choose == 1} 
            onPress={()=> {setChoose(1)}} 
            disableBuiltInState={choose !=1}
            textStyle={{color:"white", right:12, fontSize:12, textDecorationLine: 'none'}} 
            style={{right:16}}
            fillColor={Colors.secondary} 
            unfillColor={Colors.primary}
            />

        <BouncyCheckbox 
            text='Extra' 
            onPress={()=> {setChoose(2)}} 
            disableBuiltInState={choose != 2} 
            textStyle={{color:"white", right:12, fontSize:12, textDecorationLine: 'none'}} 
            style={{right:16}}
            fillColor={Colors.secondary} 
            unfillColor={Colors.primary}
            />
    </View>
  )
}

const styles = StyleSheet.create({
    container: {
        flexDirection: "row", 
    },
    text:{
        color:"white",
        marginRight:0,
    }
})