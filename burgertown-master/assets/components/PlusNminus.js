import { StyleSheet, Text, View, TouchableOpacity } from 'react-native'
import { AntDesign } from '@expo/vector-icons';
import React, { useState } from 'react'

export default function PlusNminus() {
  let number = 0;
  const [num, setNum] = useState(number);

  const minus = () =>{
    if (num > 0) {
      setNum(num-1)
    } 
    return num
  }

  return (
    <View style={styles.bottomNav}>
      <TouchableOpacity onPress={() => ( minus())}>
        <AntDesign name="minuscircleo" size={24} color="black" />
      </TouchableOpacity>

      <Text style={styles.number}> {num}</Text>

      <TouchableOpacity onPress={() => (setNum(num+1))}>
        <AntDesign name="pluscircleo" size={24} color="black" />
      </TouchableOpacity>
  </View>
  )
}

const styles = StyleSheet.create({
   bottomNav: {
    flexDirection: "row",
    justifyContent:"space-around",
    alignSelf: "flex-end",
    padding: 5,
    width: "65%"
  },
  number:{
    fontSize: 18
   },
})