import { StyleSheet, Text, View,FlatList } from 'react-native'
import React from 'react'
import EditMenue from "../components/EditMenuList"
import Colors from '../config/Colors'

export default function BackgroundLayout({title, data, choose}) {
  return (
    <View style={styles.container}>
      <Text style ={styles.title}>{title}</Text>
      {/* <FlatList nestedScrollEnabled  {...other}/> */}
      {data.map(item => (
      <View key={item.id}>
        <EditMenue
         name={item.name}
         price={item.price}
         image={item.image}
         choose={choose}
        />
      </View>))}
    </View>
  )
}

const styles = StyleSheet.create({
  container:{
    paddingTop: 20,
    paddingBottom: 20,
    marginBottom: 15,
    backgroundColor: "#6485f5",
    width:"95%",
    alignSelf: "center",
    borderRadius: 45,
  },
  title:{
    fontSize:30,
    paddingBottom: 10,
    textAlign: "center",
    color: "white"

  }
})