import { View, StyleSheet, Text, TouchableOpacity } from "react-native";
import React, { useState } from "react";
import colors from "../config/Colors";

export default function Button({
  label,
}) {
  return (
    <View>
      <TouchableOpacity 
        onPress={() => console.log("touch")}>
        <Text style={styles.buttonText}>
          {label}
        </Text>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  button:{
    borderRadius:35,
    height: "%",
    // width: 65
  },
  buttonText: {
    fontSize: 15,
    // width:"50",
  },
});
