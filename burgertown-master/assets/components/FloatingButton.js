import { StyleSheet, Text, View, TouchableOpacity } from 'react-native'
import React from 'react'

export default function FloatingButton({title}) {
  return (
   
    <TouchableOpacity style={styles.conatiner}>
      <View > 
        <Text style ={{ textAlign: "center"}}> 
            {title}
        </Text>  
      </View>
    </TouchableOpacity>

  )
}

const styles = StyleSheet.create({
    conatiner:
    {
        width: 100,  
        height: 45, 
        borderRadius: 20,  
        backgroundColor: '#ee6e73',       
        position: 'absolute', 
        justifyContent: "center",  
        alignSelf: "center",                                   
        bottom: 0,
        right:0,
        marginRight: 20,
        marginTop: 20,
        marginBottom: 20,
    }

})